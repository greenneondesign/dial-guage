# DialGuage

## About
DialGuage is a ground up reimplementation of the jsguage library: http://code.google.com/p/jsgauge/ which draws a DialGuage in an HTML Canvas element. This reimplementation recasts the code as an ES6 class inorder to make it more readable and hopefully easier to support. A future goal for the project is to create a web component vesion.

## License
This project is licensed under the MIT License - see https://opensource.org/licenses/MIT for details.

## Usage
The file example.html (included) shows examples of how to use this library. Briefly, You will need a <canvas> element in your html which you have given an id, height and width. Then in a script element you import the library and add an on load event callback which creates a new instance of the DialGauge class using your canvas element and desired options.

<html>
    ...
    <body>
        ...
        <canvas id="guage" width="250" height="250"></canvas>
        ...
        <script type="module">
			import {DialGauge} from "./DialGauge.min.js";

			function initGuages(loadEvent) {
				// Draw the gauge using default settings
                let options = {majorTickLabel: true};
				let gauge = new DialGauge(document.getElementById('guage'), 0.0, options);
			}
			
			window.addEventListener("load", initGuages);
		</script>
    </body>
</html>

###Options 
Your options dictionary can have the following key:values pairs:

    - valueFormat: a function which will be used to format the value when displaying as text. default: null
	- label: a string which will be be displayed as the guage label. default: ''
	- unitsLabel: a string which will be used as the units for values. default: ''
	- min: a number which is the minimum value the guage can display. default: 0
	- max: a number which is the maximum value the guage can display. default: 100,
	- majorTicks: a number which specifies the number of major tick marks to draw. default: 5
	- minorTicks: a number which specifies the number of minor tick marks to draw between each major tick mark. default: 2,
	- majorTickLabel: a boolean specifying whether to draw labels for major ticks. default: false
	- decimals: an integer which specifies the number of places past the decimal point at which values will be rounded. default: 0
    - text: a css color which will be used to color text normally. default: 'rgb(0, 0, 0)',
	- warningText: a css color which will be used to color the value text when the value overflows or underflows the guage range. default: 'rgb(255, 0, 0)',
	- fill: an array of four colors which will be used to color the background of the dial. default: [ '#111', '#ccc', '#ddd', '#eee' ],
	- pointerFill: a css color which will be used to fill the dial pointer. default: 'rgba(255, 100, 0, 0.7)',
	- pointerStroke: a css color which will be used to outline (stroke) the dial pointer. default: 'rgba(255, 100, 100, 0.9)',
	- centerCircleFill: a css color which will be used to color the circle where the dial pointer pivots. default: 'rgba(0, 100, 255, 1)',
	- colorOfCenterCircleStroke: a css color which will be used to outline (stroke) the circle where the dial pointer pivots. default: 'rgba(0, 0, 255, 1)',
    - bands: an array of bands to draw. default: []. Bands are specified as dictionaries of:
        - color: the color of the band
        - from: the value at which the band starts
        - to: the value where the band ends
        all values in a band dictionary are required (no defaults).
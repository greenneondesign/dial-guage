/**
 *	DialGauge is an ES6 class for drawing a dial guage in an HTMLCanvasElement. It is inspired
 *	by the jsguage library: http://code.google.com/p/jsgauge/
 *
 *	@author Tom Egan <tkegan@greenneondesign.com>
 *
 *	@license: MIT License see: http://www.opensource.org/licenses/mit-license.php
 */
export class DialGauge {
	/**
	 *	Create a dial guage from the provided HTMLCanvasElement
	 *
	 *	@param {HTMLCanvasElement} canvas - The gauge will be drawn inside this element
	 *	@param {float} value - the intial value to be displayed by the gauge
	 *	@param {Object} options - A dictionary of settings configuring the appearance of the guage
	 *	@constructor
	 */
	constructor(canvas, value = 0.0, options = {}) {
		this.canvas = canvas;
		if(!this.canvas.getContext) {
			throw new TypeError("canvas element must support 2D drawing context e.g. HTMLCanvasElement");
		}
		
		//these should be constants...
		this.arcSpan = Math.PI * 13 / 8;
		this.arcStart = Math.PI * 11 / 16;
		this.arcStop = Math.PI * 5 / 16;

		this.setOptions(options);

		// we have to have a sane state to start
		this.value = this.settings.min;
		this.pointerValue = this.settings.min;
		this.animationToken = null;

		this.setValue(value);

		// make sure we do an initial draw even if all values are defaulted
		if((this.settings.min - this.value) <= 0.001) {
			this.draw();
		}
	}
	
	/**
	 *	Set the value to be indicated by the gauge needle.
	 *
	 *	@param {float} value - the value to be displayed by the gauge. If the value is less
	 *		than the minimum value for the gauge, it will be pulled up to the minimum. If
	 *		the value is greater than the maximum value for the gauge, it will be pulled
	 *		down to the maximum.
	 */
	setValue(value) {
		// Clear animation callback
//		if(this.animationToken !== null) {
//			window.cancelAnimationFrame(this.animationToken);
//			this.animationToken = null;
//		}

		value = parseFloat(value);
		if(!isNaN(value)) {
			let oldValue = this.value;
			let oldOverflow = this.overflow;
			
			if(value > this.settings.max) {
				this.value = this.settings.max;
				this.overflow = true;
			} else if(value < this.settings.min) {
				this.value = this.settings.min;
				this.overflow = true;
			} else {
				this.value = value;
				this.overflow = false;
			}
			
			this.pointerValue = (this.value - this.settings.min) / (this.settings.max - this.settings.min);

			if(Math.abs(oldValue - this.value) > 0.001 || oldOverflow != this.overflow) {
				// only redraw if value changed
				// actually we should animate but that's a future feature
				this.draw();
			}
		} else {
			throw new TypeError("DialGauge value must be a floating point number");
		}
	}
	
	/**
	 *	Set the appearance based on a dictionary of settings
	 *
	 *	@param {Dictionary} options
	 */
	setOptions(options) {
		// Gauge settings
		this.settings = {
			valueFormat: options.valueFormat || null,
			label: options.label || '',
			unitsLabel: options.unitsLabel || '',
			min: options.min || 0,
			max: options.max || 100,
			majorTicks: options.majorTicks || 5,
			minorTicks: options.minorTicks || 2,
			bands: [].concat(options.bands || []),
			majorTickLabel: options.majorTickLabel || false,
			decimals: options.precision || 0,
		};

		// Fix some possible invalid settings
		this.settings.majorTicks = Math.max( 2, this.settings.majorTicks );
		this.settings.minorTicks = Math.max( 0, this.settings.minorTicks );
		this.settings.decimals = Math.max( 0, this.settings.decimals);

		// Colors used to render the gauge
		this.colors = {
			text: options.colorOfText || 'rgb(0, 0, 0)',
			warningText: options.colorOfWarningText || 'rgb(255, 0, 0)',
			fill: options.colorOfFill || [ '#111', '#ccc', '#ddd', '#eee' ],
			pointerFill: options.colorOfPointerFill || 'rgba(255, 100, 0, 0.7)',
			pointerStroke: options.colorOfPointerStroke || 'rgba(255, 100, 100, 0.9)',
			centerCircleFill: options.colorOfCenterCircleFill || 'rgba(0, 100, 255, 1)',
			centerCircleStroke: options.colorOfCenterCircleStroke || 'rgba(0, 0, 255, 1)',
		};

		// Normalize to a [0, 100] interval
		let spanPct = (this.settings.max - this.settings.min);

		// Restrict pointer to range of values
		if(this.value > this.settings.max){
			this.value = this.settings.max;
		} else if(this.value < this.settings.min){
			this.value = this.settings.min;
		}

		// internally we use a normalized space to make drawing math easier
		this.pointerValue = (this.value - this.settings.min) / spanPct;

		this.normalizedBands = [];
		for(let i=0, len = this.settings.bands.length; i < len; i++) {
			let band = this.settings.bands[i];
			this.normalizedBands.push({
				color: band.color,
				from: (band.from - this.settings.min)/spanPct,
				to: (band.to - this.settings.min)/spanPct
			});
		}

		// should probably redraw here
	}

	/**
	 *	Format a value using a configured custom formatter or by default format
	 *	as a fixed point number with no trailing 0's
	 *
	 *	@param (Number) value - the value to format
	 *	@param (int) decimals - the number of places past the decimal to show
	 *
	 *	@return (string) the value formated as a fixed point number with up to
	 *		the provided number of digits past the decimal but no trailing 0's
	 */
	formatValue(value, decimals) {
		// Custom formatter
		if(typeof this.settings.valueFormat == 'function') {
			return this.settings.valueFormat(value, decimals);
		}

		// Default formatter
		let ret = (parseFloat(value)).toFixed(decimals);
		while((decimals > 0) && ret.match(/^(-)?\d+\.(\d+)?0$/)) {
			decimals -= 1;
			ret = (parseFloat(value)).toFixed(decimals);
		}
		return ret;
	}

	/**
	 *	Draw the Dial Gauge in the HTMLCanvasElement
	 */
	draw() {
		let c2d = this.canvas.getContext('2d');
		
		// recalculate dimensions since they may have changed since the last draw
		let width = c2d.canvas.width;
		let height = c2d.canvas.height;
		let radius = Math.min((width / 2), (height / 2)) - 4;
		let innerRadius = radius * 0.7;
		let midRadius = radius * 0.77;
		let outerRadius = radius * 0.9;
		let centerX = radius + 4;
		let centerY = height - radius * Math.sin(this.arcStop);

		// clear canvas
		c2d.clearRect(0, 0, width, height);

		// draw background
		let rad = [ radius, radius - 1, radius * 0.98, radius * 0.95 ];
		for(let i = 0, len = this.colors.fill.length; i < len; i++) {
			c2d.fillStyle = this.colors.fill[i];
			c2d.beginPath();
			c2d.arc(centerX, centerY, rad[i], this.arcStart, this.arcStop);
			c2d.fill();
		}

		// draw bands
		for(let i = 0, len = this.normalizedBands.length; i < len; i++) {
			let band = this.normalizedBands[i];
			if(band.to > band.from) {
				let start = this.arcStart + this.arcSpan * band.from;
				let end = this.arcStart + this.arcSpan * band.to;

				c2d.fillStyle = band.color;
				c2d.beginPath();
				c2d.moveTo(centerX + innerRadius * Math.cos(start), centerY + innerRadius * Math.sin(start));
				c2d.lineTo(centerX + outerRadius * Math.cos(start), centerY + outerRadius * Math.sin(start));
				c2d.arc(centerX, centerY, outerRadius, start, end, false);
				c2d.lineTo(centerX + innerRadius * Math.cos(end), centerY + innerRadius * Math.sin(end));
				c2d.arc(centerX, centerY, innerRadius, end, start, true);
				c2d.fill();
			}
		}

		// prepare to draw text
		let fontSize = radius / 5;
		c2d.font = fontSize.toFixed(0) + 'px sans-serif';
		
		// draw the caption (see: options.label) centered below the dial
		if(this.settings.label) {
			let metrics = c2d.measureText(this.settings.label).width;
			c2d.fillStyle = this.colors.text;
			c2d.fillText(this.settings.label, centerX - metrics / 2, centerY - radius * 0.4 + fontSize / 2);
		}

		// draw value
		let valueText = this.formatValue(this.value, this.settings.decimals) + this.settings.unitsLabel;
		let metrics = c2d.measureText(valueText).width;
		if(this.overflow) { // Outside min/max ranges?
			c2d.fillStyle = this.colors.warningText;
		} else {
			c2d.fillStyle = this.colors.text;
		}
		c2d.fillText(valueText, centerX - metrics / 2, centerY + radius * 0.65  + fontSize / 2);

		// prepare to draw text
		c2d.fillStyle = this.colors.text;
		fontSize = radius / 10;
		c2d.font = fontSize.toFixed(0) + 'px sans-serif';
		
		// draw ticks
		c2d.strokeStyle = 'rgb(0, 0, 0)';
		let tickInterval = (this.settings.max - this.settings.min) / (this.settings.majorTicks - 1);
		let majorTickInterval = this.arcSpan / (this.settings.majorTicks - 1);
		let minorTickInterval = 0;
		if(this.settings.minorTicks > 0) {
			minorTickInterval = majorTickInterval / (this.settings.minorTicks);
		}
		for(let i = 0; i < this.settings.majorTicks; i++) {
			let theta = this.arcStart + i * majorTickInterval;
			c2d.lineWidth = this.radius * 0.025;
			c2d.beginPath();
			c2d.moveTo(centerX + innerRadius * Math.cos(theta), centerY + innerRadius * Math.sin(theta));
			c2d.lineTo(centerX + outerRadius * Math.cos(theta), centerY + outerRadius * Math.sin(theta));
			c2d.stroke();


			let tickLabelText = this.formatValue(this.settings.min + i * tickInterval, this.settings.decimals)
			metrics = c2d.measureText(tickLabelText).width;
			let normalizedTheta = i* majorTickInterval / this.arcSpan;
			c2d.fillText(tickLabelText, centerX + 0.6 * radius * Math.cos(theta) - normalizedTheta * metrics, centerY + 0.6 * radius * Math.sin(theta) + fontSize * Math.sin(Math.PI * normalizedTheta));

			// minor ticks
			if(this.settings.minorTicks > 0 && (i + 1) < this.settings.majorTicks) {
				c2d.lineWidth = this.radius * 0.01;
				for(let j = 0; j < this.settings.minorTicks; j++) {
					c2d.beginPath();
					c2d.moveTo(centerX + innerRadius * Math.cos(theta + j * minorTickInterval), centerY + innerRadius * Math.sin(theta + j * minorTickInterval));
					c2d.lineTo(centerX + midRadius * Math.cos(theta + j * minorTickInterval), centerY + midRadius * Math.sin(theta + j * minorTickInterval));
					c2d.stroke();
				}
			}
		}

		// draw the pointer
		c2d.lineWidth = this.radius * 0.025;
		let pointerAngle = this.arcStart + this.arcSpan * this.pointerValue;
		c2d.fillStyle = this.colors.pointerFill;
		c2d.strokeStyle = this.colors.pointerStroke;
		c2d.beginPath();
		c2d.moveTo(centerX - 0.05 * radius * Math.cos(pointerAngle + Math.PI / 2), centerY - 0.05 * radius * Math.sin(pointerAngle + Math.PI / 2));
		c2d.lineTo(centerX + midRadius * Math.cos(pointerAngle), centerY + midRadius * Math.sin(pointerAngle));
		c2d.lineTo(centerX - 0.05 * radius * Math.cos(pointerAngle - Math.PI / 2), centerY - 0.05 * radius * Math.sin(pointerAngle - Math.PI / 2));
		c2d.lineTo(centerX - 0.5 * midRadius * Math.cos(pointerAngle), centerY - 0.5 * midRadius * Math.sin(pointerAngle));

		c2d.fill();
		c2d.stroke();
		
		c2d.fillStyle = this.colors.centerCircleFill;
		c2d.strokeStyle = this.colors.centerCircleStroke;
		c2d.beginPath();
		c2d.arc(centerX, centerY, radius * 0.1, 0, Math.PI * 2, true);
		c2d.fill();
		c2d.stroke();
	}
};